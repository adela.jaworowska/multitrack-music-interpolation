
""" Starting size of the app window """
WINDOW_WIDTH = 1000
WINDOW_HEIGHT = 600
SETTINGS_MENU_PANE_WIDTH = 160
SAMPLE_PANE_WIDTH = (WINDOW_WIDTH-SETTINGS_MENU_PANE_WIDTH)/2

LATENT_SPACE_SIZE = 1024

""" Directory path to files to play music form and to interpolate them """
SAMPLES_DIR = "../GUI/exampleSamples/"

""" Directory and file with interpolated output """
INTERPOLATED_DIR = "../GUI/interpolationOutput/"

""" Directory to pre-trained neural network models' weights """
MODEL_WEIGHTS = "./model/weights/"
AE_LSTM_MODEL_WEIGHTS = "./model/weights/AE LSTM.h5"
AE_GRU_MODEL_WEIGHTS = "./model/weights/AE GRU.h5"
VAE_LSTM_MODEL_WEIGHTS = "./model/weights/VAE LSTM.h5"
VAE_GRU_MODEL_WEIGHTS = "./model/weights/VAE GRU.h5"
VAE_LSTM_LOFI_WEIGHTS = "./model/weights/lofi_VAE LSTM.h5"

MODELS = [
    "AE LSTM",
    "AE LSTM",
    "AE GRU",
    "VAE LSTM",
    "VAE GRU",
    "LO-FI",
]

INTERPOLATION = [
    "LERP",
    "LERP",
    "SLERP"
]

""" Colors """
deep_magenta = "#740048"
light_gray = "#666666"
gray = "#E0E0E0"
light_blue = "#FAEEF5"
