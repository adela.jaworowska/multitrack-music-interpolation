import tkinter as tk
from window import Window
from config import *
from ttkthemes import themed_tk as tk
from tkinter import ttk

# root = tk.Tk()
root = tk.ThemedTk()
print(root.get_themes())
root.set_theme("clearlooks")

#size of the window
root.geometry(str(WINDOW_WIDTH) + "x" + str(WINDOW_HEIGHT))
root.resizable(height=False, width=False)

app = Window(root)
root.mainloop()
