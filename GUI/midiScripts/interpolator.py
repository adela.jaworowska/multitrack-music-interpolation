import numpy as np
import pypianoroll
import pretty_midi
from midiScripts.midi_preprocessing import MidiPreprocessor

class Interpolator:
    """Class  used to perform interpolation"""

    def __init__(self):
        self.mp = MidiPreprocessor()
        self.is_VAE = False

    @staticmethod
    def linear_interpolation(encoded_sample_1, encoded_sample_2, interpolation_length):
        """Linear interpolation, generates sequence of vectors (as many as interpolation length)"""
        mi = np.expand_dims(np.linspace(0, 1, interpolation_length), 1)
        return [np.reshape(np.array(e), encoded_sample_1.shape) for e in ((1 - mi) * encoded_sample_1 + mi * encoded_sample_2)]

    def _slerp(self, p0, p1, t):
        """Spherical linear interpolation, generates one vector"""
        omega = np.arccos(
            np.dot(np.squeeze(p0 / np.linalg.norm(p0)),
                   np.squeeze(p1 / np.linalg.norm(p1))))
        so = np.sin(omega)
        return np.sin((1.0 - t) * omega) / so * p0 + np.sin(t * omega) / so * p1

    def spherical_interpolation(self, encoded_sample_1, encoded_sample_2, interpolation_length):
        """Spherical linear interpolation, generates sequence of vectors (as many as interpolation length)"""
        print(encoded_sample_1)
        print(encoded_sample_2)
        if (encoded_sample_1 == encoded_sample_2).all() or None in encoded_sample_1 or None in encoded_sample_2:
            return np.tile(encoded_sample_1, (interpolation_length, 1))
        else:
            return np.array([self._slerp(encoded_sample_1, encoded_sample_2, t) for t in np.linspace(0, 1, interpolation_length)])

    def interpolate(self, sample_1_path, sample_2_path, encoder, decoder, interpol_length, interpolation_type, interpolation_concatenated_output_dir, output_file_name, tempo_auto, tempo=None):
        samp1 = self.mp.load_data(sample_1_path)
        samp2 = self.mp.load_data(sample_2_path)
        samp1 = np.reshape(samp1, (1, 96, 6, 128))
        samp2 = np.reshape(samp2, (1, 96, 6, 128))

        if not self.is_VAE:
            samp1_encoded = encoder.predict(samp1)
            samp2_encoded = encoder.predict(samp2)
        else:
            samp1_encoded = encoder.predict(samp1)[2]
            samp2_encoded = encoder.predict(samp2)[2]

        if interpolation_type == "LERP":
            sequence = self.linear_interpolation(samp1_encoded, samp2_encoded, interpol_length)
        else:
            sequence = self.spherical_interpolation(samp1_encoded, samp2_encoded, interpol_length)

        decoded = []
        for elem in sequence:
            temp = decoder.predict([elem, samp2])
            temp = np.reshape(temp, (1, 6, 96, 128))
            decoded.append(temp[0])
        decoded = np.stack(decoded, 1)
        interpolated_result = np.reshape(decoded, (6, len(sequence)*96, 128))

        if tempo_auto:
            output_tempo = self.get_output_tempo(sample_1_path, sample_2_path, interpol_length)
        else:
            output_tempo = np.full((96 * interpol_length, 1), tempo)
        try:
            self.mp.convert_nn_output_to_midi_file(interpolated_result, interpolation_concatenated_output_dir + output_file_name, output_tempo)
        except:
            raise NameError("Error while saving output MIDI file. ")

    def get_output_tempo(self, input_midi1, input_midi2, interpolation_len):
        input_tempo1 = pypianoroll.from_pretty_midi(pretty_midi.PrettyMIDI(input_midi1)).tempo
        input_tempo2 = pypianoroll.from_pretty_midi(pretty_midi.PrettyMIDI(input_midi2)).tempo
        input_tempo1 = np.reshape(input_tempo1, (1, len(input_tempo1)))
        input_tempo2 = np.reshape(input_tempo2, (1, len(input_tempo2)))
        interpolated_tempo = self.linear_interpolation(input_tempo1, input_tempo2, interpolation_len)
        interpolated_tempo = np.concatenate(np.concatenate(interpolated_tempo))
        return np.reshape(interpolated_tempo, (len(interpolated_tempo), 1))
