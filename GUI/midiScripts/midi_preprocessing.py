import pretty_midi
import glob
from tqdm import tqdm
import shutil
import os
import numpy as np
import pypianoroll
from pypianoroll import StandardTrack
import copy


class MidiPreprocessor:
    """Class used to preprocess MIDI data"""

    index_to_program_dict = {0: 0, 1: 25, 2: 30, 3: 33, 4: 48, 5: 128}  # maps index of array that is input for nn model to midi program number
    programs_list = [0, 25, 30, 33, 48, 128]  # List of midi programs considered in this project
    number_of_instruments = 6

    """ 
    All methods to line 145 are the most meaningful methods  
    They are used while application is running to convert data to proper formats.
    """

    @classmethod
    def mark_drum_as_128_program_number(cls, multitrack):
        """
        For the project purposes - in multitrack object, if the instrument is drum, program number is marked with 128.

        :param multitrack
        :return: multitrack with drums marked with 128 program number
        """
        for t in multitrack:
            if t.is_drum:
                t.program = 128
        return multitrack

    @classmethod
    def create_tracks_pianoroll_dict_by_program_number(cls, multitrack):
        """
        For the project purposes - based on multitrack object, creates pianoroll for each track

        :param multitrack
        :return: dictionary of pianorolls for each track
        """
        tracks_dict = {}
        for t in multitrack.tracks:
            tracks_dict[t.program] = t.pianoroll
        return tracks_dict

    @classmethod
    def convert_midi_file_to_matrix(cls, file, append_tempo = False):
        """
        Converts MIDI file to matrix that can be passe to nn model

        :param file: path to MIDI file that is converted to matrix
        :return: one data sample converted to matrix - proper format for nn
        """

        song = pretty_midi.PrettyMIDI(file)
        multitrack = pypianoroll.from_pretty_midi(song)
        multitrack = cls.mark_drum_as_128_program_number(multitrack)

        # blend tracks if program is the same
        multitrack = cls.blend_tracks_with_same_program(multitrack)

        programs = cls.programs_list
        pianorolls = []
        tracksDict = cls.create_tracks_pianoroll_dict_by_program_number(multitrack)
        for p in programs:
            if p in tracksDict:
                pianorolls.append(tracksDict[p])
            else:
                pianorolls.append(np.zeros((multitrack.tempo.shape[0], 128)))

        if append_tempo:
            pianorolls.append(multitrack.tempo)

        output_matrix = np.stack(pianorolls)
        output_matrix[output_matrix > 0] = 1  # Normalize data
        return output_matrix

    @classmethod
    def blend_tracks_with_same_program(self, multitrack):
        """
        Blends pianorolls from instruments tracks into one pianoroll if the program number is the same

        :param multitrack: multitrack object that tracks are checked and blended if needed
        :return: multitrack with blended tracks if it was needed
        """

        oldTracks = multitrack.tracks
        programs = set()
        for t in oldTracks:
            programs.add(t.program)

        # tak only one stack with particular program number
        # later it's pianoroll will be replaced with stacked one
        tempTracks = {}
        for p in programs:
            for track in oldTracks:
                if track.program == p:
                    tempTracks[p] = track
                    break

        for p in programs:
            tracksToStack = []
            for t in oldTracks:
                if t.program == p:
                    tracksToStack.append(t)
            tempMultitrack = multitrack
            tempMultitrack.tracks = tracksToStack
            stackedTracksPianoroll = tempMultitrack.stack()
            tempTracks[p].pianoroll = np.max(stackedTracksPianoroll, axis=0)

        # assign new blended tracks to multitrack object
        multitrack.tracks = []
        for programNumber in tempTracks:
            multitrack.tracks.append(tempTracks[programNumber])

        return multitrack

    @classmethod
    def load_data(cls, data_dir, append_tempo = False):
        """
        Loads all the data from the given directory to the program.
        It can by used during neural network training if it is possible to load all data at once to the program.

        :param data_dir: path to directory with data samples
        :param: append_tempo: indicates if tempo should be appended while loading data
        :return: one data sample converted to proper format for nn
        """
        dataset = []
        for file in tqdm(glob.glob(data_dir, recursive=True)):
             dataset.append(cls.convert_midi_file_to_matrix(file, append_tempo))
        return np.array(dataset)

    @classmethod
    def samples_generator_for_nn_training(cls, data_directory):
        """
        This is generator function used in tensorflow.data.Dataset.from_generator formula.
        It is used during neural network training to provide samples for training.

        :param data_directory: path to directory with data samples
        :return: one data sample converted to proper format for nn
        """
        for file in glob.glob(data_directory + "*.mid"):
            notes = cls.convert_midi_file_to_matrix(file, False)

            yield np.array(notes)

    @classmethod
    def convert_nn_output_to_multitrack_pianoroll(cls, nn_output_matrix):
        """
        Normalises the data in matrix returned by nn model before converting it to MIDI  file

        :param nn_output_matrix: matrix returned by neural network model
        :return normalised output matrix
        """
        nn_output_matrix = nn_output_matrix *127
        nn_output_matrix[nn_output_matrix < 20] = 0
        np.around(nn_output_matrix)
        return nn_output_matrix

    @classmethod
    def convert_nn_output_to_midi_file(cls, nn_output_matrix, output_file_name, output_tempo=None):
        """
        Converts matrix returned by nn model to MIDI file.

        :param nn_output_matrix: matrix returned by neural network model
        :param output_file_name: name of output MIDI file
        :param output_tempo: tempo of output MIDI, default is None
        :return: one data sample converted to proper format for nn
        """
        index_to_program_dict = cls.index_to_program_dict
        pianoroll = cls.convert_nn_output_to_multitrack_pianoroll(nn_output_matrix)

        output_multitrack = pypianoroll.Multitrack(tempo=output_tempo)
        print(output_multitrack)
        for i, elem in zip(range(0, len(index_to_program_dict)), index_to_program_dict):
            new_track = StandardTrack()
            new_track.pianoroll = pianoroll[i]
            if index_to_program_dict[i] == 128:
                new_track.program = 0
                new_track.is_drum = True
            else:
                new_track.program = index_to_program_dict[i]
                new_track.is_drum=False
            output_multitrack.tracks.append(new_track)
        output_multitrack.write(output_file_name)


    """ 
    All methods below are just helping methods. 
    They were mainly used for initial data analysis and pre-processing.
    """

    @classmethod
    def get_instrument_name_from_program(cls, program_number):
        return pretty_midi.instrument_name_to_program(program_number)

    @classmethod
    def get_program_from_instrument_name(cls, instrument_name):
        return pretty_midi.instrument_name_to_program(instrument_name)

    @classmethod
    def prepare_music_samples_of_given_length(cls, data_dir, length, offset):
        """Modification of the offset can be used for data augmentation"""
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                song = pretty_midi.PrettyMIDI(file)
                fileName = file.split('\\')[-1]

                multitrack = pypianoroll.from_pretty_midi(song)
                song_length = len(multitrack.tempo)

                new_multitrack = copy.deepcopy(multitrack)
                new_multitrack.downbeat = None
                for i in range(0, song_length - length - 1, offset):
                    new_multitrack.tempo = multitrack.tempo[i:length + i]
                    for t, nt in zip(multitrack.tracks, new_multitrack.tracks):
                        nt.pianoroll = t.pianoroll[i:length + i]
                    pypianoroll.write(
                        "F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_test_dataset_len96/" + str(i) + "_" + fileName,
                        new_multitrack)
            except:
                pass

    @classmethod
    def count_number_of_samples_by_length(cls, data_dir):
        exceptions = 0
        samples_dict_by_length = {}

        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                song = pretty_midi.PrettyMIDI(file)
                multitrack = pypianoroll.from_pretty_midi(song)
                length = len(multitrack.tempo)

                if length in samples_dict_by_length:
                    samples_dict_by_length[length] = samples_dict_by_length[length]+1
                else:
                   samples_dict_by_length[length] = 1
            except:
                exceptions += 1
                pass
        # Save number of samples of given length to file
        f = open("numberOfSamplesByLength_MUZA_MIDI.txt", "w+")
        f.write("Length: num_of_samples \n")
        for elem in samples_dict_by_length:
            f.write(str(elem) + ": " + str(samples_dict_by_length[elem]) + "\n")
        f.write("Exceptions: " + str(exceptions))
        f.close()

    @classmethod
    def count_number_of_samples_by_number_of_non_zero_values(cls, data_dir, accepted_sample_length):
        exceptions = 0
        samples_dict_by_non_zero_values = {}

        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                song = pretty_midi.PrettyMIDI(file)
                multitrack = pypianoroll.from_pretty_midi(song)
                if len(multitrack.tempo) != accepted_sample_length:
                    shutil.move(file, "F:/ADELA/studia/MAGISTERKA/data/errors_shortSamples_MUZA_MIDI/")
                    continue

                matrix = cls.convert_midi_file_to_matrix(file)
                length = np.count_nonzero(matrix)

                if length in samples_dict_by_non_zero_values:
                    samples_dict_by_non_zero_values[length] = samples_dict_by_non_zero_values[length]+1
                else:
                    samples_dict_by_non_zero_values[length] = 1
            except:
                exceptions += 1
                shutil.move(file, "F:/ADELA/studia/MAGISTERKA/data/errors_shortSamples_MUZA_MIDI/")

        # Save number of samples of given length to file
        f = open("numberOfSamplesByNumberOfNonZeroValues_MUZA_MIDI.txt", "w+")
        f.write("Num_of_non_zero_values: num_of_samples \n")
        for elem in sorted(samples_dict_by_non_zero_values.keys()):
            f.write(str(elem) + ": " + str(samples_dict_by_non_zero_values[elem]) + "\n")
        f.write("Exceptions: " + str(exceptions))
        f.close()

    @classmethod
    def cut_music_smaples_by_beat(cls, data_dir, output_dir):
        ''' data_dir: "F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_chosenSongsWithOnly5ChosenInstruments/*"
            output_dir: "F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_withDownbeatArray_cutByBeat/"
        '''
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            fileName = file.split('\\')[-1]
            song = pretty_midi.PrettyMIDI(file)
            multitrack = pypianoroll.from_pretty_midi(song)

            # Get indexes of beats' start
            unique, counts = np.unique(multitrack.downbeat, return_counts=True)
            di = dict(zip(unique, counts))
            beatStarts = np.where(multitrack.downbeat == True)
            new_multitrack = copy.deepcopy(multitrack)
            for i in range(0, len(beatStarts[0]) - 1):
                new_multitrack.downbeat = multitrack.downbeat[beatStarts[0][i]:beatStarts[0][i + 1] - 1]
                new_multitrack.tempo = multitrack.tempo[beatStarts[0][i]:beatStarts[0][i + 1] - 1]
                for t, nt in zip(multitrack.tracks, new_multitrack.tracks):
                    nt.pianoroll = t.pianoroll[beatStarts[0][i]:beatStarts[0][i + 1] - 1]

                    pypianoroll.write(output_dir + str(i) + "_" + fileName, new_multitrack)

    @classmethod
    def mergeMidiFiles(cls, data_dir):

        number_of_files = len(glob.glob(data_dir))
        output_multitrack = pypianoroll.Multitrack()

        for prog in cls.programs_list:
            track = StandardTrack()
            track.program = prog
            track.pianoroll = np.zeros((96 * number_of_files, 128))
            pianorolls_to_concatenate = []
            for file, i in zip(glob.glob(data_dir, recursive=True), range(0, 96*number_of_files, 96)):
                isProgramInFile = False
                song = pretty_midi.PrettyMIDI(file)
                multitrack = pypianoroll.from_pretty_midi(song)
                for t in multitrack:
                    if(prog == 128 and t.is_drum):
                        isProgramInFile = True
                        track.pianoroll[i:i + 96] = t.pianoroll

                    else:
                        if(t.program == prog):
                            isProgramInFile = True
                            print("TRUE")
                            print((track.pianoroll).shape)
                            print((t.pianoroll).shape)
                            track.pianoroll[i:i+96] = t.pianoroll

            if(track.program == 128):
                track.program = 0
                track.is_drum = True
            output_multitrack.tracks.append(track)
        output_multitrack.tempo = np.full((96*7, 1), 99.15110132)
        print(output_multitrack)
        for t in multitrack:
            if t.is_drum:
                t.name = t.program
            else:
                t.name = "drum"
        pypianoroll.write("F:/ADELA/studia/MAGISTERKA/data/toMerge/merged_Tempo.mid", output_multitrack)

    def move_to_separate_songs_with_4_4_time_signature(self, input_data_dir):
        for file in tqdm(glob.glob(input_data_dir, recursive=True)):
            song = pretty_midi.PrettyMIDI(file)
            if(len(song.time_signature_changes) == 1 and song.time_signature_changes[0].numerator == 4 and song.time_signature_changes[0].denominator == 4):
                shutil.copy(file, "F:/ADELA/studia/MAGISTERKA/data/various_4_4_timeSignature/")

    def prepare_test_dataset_MUZA_MIDI(self, programsToCheckSet):
        counter = 0
        for file in tqdm(glob.glob("F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_test_dataset_len96/*.MID", recursive=True)):
            try:
                song = pretty_midi.PrettyMIDI(file)
                multitrack = pypianoroll.from_pretty_midi(song)
                if len(multitrack.tempo) != 96:
                    os.remove(file)
                    counter += 1
            except:
                os.remove(file)
                counter += 1
        print(counter)

    @classmethod
    def count_number_of_songs_with_number_of_channels_in_range(cls, min, max, data_dir):
        counter = 0
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            print(file)
            try:
                midi = pretty_midi.PrettyMIDI(file)
                if(len(midi.instruments) >= min and  len(midi.instruments) <= max):
                    counter += 1
            except:
                shutil.move(file, "F:/ADELA/studia/MAGISTERKA/data/errors/")

        return counter

    @classmethod
    def move_songs_that_generate_errors_to_other_dir(cls, data_dir):
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                midi = pretty_midi.PrettyMIDI(file)
                l = len(midi.instruments)
            except:
                try:
                    shutil.move(file, "F:/ADELA/studia/MAGISTERKA/data/errors/")
                except:
                    os.remove(file)

    @classmethod
    def countNumberOfSongsWithGivenNumberOfPrograms(cls, data_dir):
        """Returns an array of length 128.
        In cell array[index] there is a number of songs that consist 'index' programs"""
        songs_number = 150 * [0]
        exceptions = 0
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                midi = pretty_midi.PrettyMIDI(file)
                l = len(midi.instruments)
                songs_number[l] += 1
            except:
                exceptions += 1
                pass

        f = open("songsWithGivenNumOfPrograms_MUZA_MIDI_byBeat.txt", "w+")
        for i in range(128):
            f.write(str(i) + ": " + "%d\r\n" % (songs_number[i]))

        f.close()
        return songs_number

    @classmethod
    def count_occurrences_of_particular_instrument(cls, data_dir):
        instruments_number = 128 * [0]
        exceptions = 0

        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                midi = pretty_midi.PrettyMIDI(file)
                instruments = midi.instruments
                for inst in instruments:
                    instruments_number[inst.program] += 1
            except:
                exceptions += 1
                pass

        f = open("numberOfOccurrenceOfMidiProgram_MUZA_MIDI_byBeat.txt", "w+")
        for i in range(128):
            f.write(str(i) + ": " + "%d\r\n" % (instruments_number[i]))
        f.close()
        return instruments_number

    @classmethod
    def count_occurrences_of_particular_instrument_with_drum(cls, data_dir):
        instruments_number = 129 * [0]
        exceptions = 0

        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                midi = pretty_midi.PrettyMIDI(file)
                instruments = midi.instruments
                for inst in instruments:
                    if not inst.is_drum:
                        instruments_number[inst.program] += 1
                    else:
                        instruments_number[128] += 1
            except:
                exceptions += 1
                pass

        f = open("numberOfOccurrenceOfMidiProgramWithDrum_various_4_4.txt", "w+")
        for i in range(129):
            if (i < 128):
                f.write(str(i) + ", " + str(pretty_midi.program_to_instrument_name(i)) + ": " + "%d\r\n" % (instruments_number[i]))
            else:
                f.write(str(i) + ", drum" + ": " + "%d\r\n" % (instruments_number[i]))
        f.close()
        return instruments_number

    @classmethod
    def get_song_time_in_seconds(cls, midi_song):
        return midi_song.get_end_time()

    @classmethod
    def get_song_time_in_ticks(cls, midi_song):
        return midi_song.time_to_tick(midi_song.get_end_time())

    @classmethod
    def print_names_of_instruments_in_programs(cls):
        for i in range(0, 128):
            print(str(i) + ": " + pretty_midi.program_to_instrument_name(i))

    @classmethod
    def sort_instruments_by_number_of_occurrence(cls, input_txt_file):
        ''' Eg. Input "occurrencesOfMidiProgramsWithDrum.txt" '''
        file1 = open(input_txt_file, 'r')
        lines = file1.readlines()

        program_dict = dict()
        for line in lines:
            if line == "\n":
                continue
            key = int(line.split(":")[0])
            value = (line.split(":")[1]).strip()
            if key < 128:
                instrument = pretty_midi.program_to_instrument_name(key)
            else:
                instrument = "drum"
            program_dict[instrument] = int(value)

        program_dict = {k: v for k, v in sorted(program_dict.items(), key=lambda item: item[1])}
        sorted_tuples = sorted(program_dict.items(), key=lambda item: item[1])
        program_dict = {k: v for k, v in sorted_tuples}

        f = open("sorted" + input_txt_file, "w+")
        for elem in program_dict:
            f.write(str(elem) + ": " + str(program_dict[elem]) + "\n")
        f.close()
        return program_dict


    @classmethod
    def find_song_for_test(self, data_dir):

        for file in tqdm(glob.glob(data_dir, recursive=True)):
            try:
                midi = pretty_midi.PrettyMIDI(file)
                instruments = midi.instruments
                for inst in instruments:
                    pass
            except:
                pass

    @classmethod
    def get_number_of_chosen_instruments_in_song(cls, file, programsToCheckSet):
        ''' Checks how many programs from programsToCheckSet is in a given file '''

        midi = pretty_midi.PrettyMIDI(file)
        instruments = midi.instruments

        programsSet = set()
        for inst in instruments:
            if not inst.is_drum:
                programsSet.add(inst.program)
            elif inst.is_drum:
                programsSet.add(128)

        return len(programsToCheckSet.intersection(programsSet))

    @classmethod
    def fileContainsAllChosenInstruments(cls, file, programsToCheckSet):
        """I assume the drum as program with number 128"""
        midi = pretty_midi.PrettyMIDI(file)
        instruments = midi.instruments

        programsSet = set()
        for inst in instruments:
            if not inst.is_drum:
                programsSet.add(inst.program)
            elif inst.is_drum:
                programsSet.add(128)

        if programsToCheckSet.issubset(programsSet):
            print("All: " + str(programsSet))
            print("To check: " + str(programsToCheckSet))
        return programsToCheckSet.issubset(programsSet)

    @classmethod
    def separate_songs_for_experiments(cls, data_dir):
        """
        If song contains all instruments listed below copy that song to separate folder.
        33 Electric Bass (finger): 24413
        25 Acoustic Guitar (steel): 25945
        48 String Ensemble 1: 42777
        0 Acoustic Grand Piano: 110622
        128 (I assume the drum as program 128) drum: 110847
        """
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            if (cls.get_number_of_chosen_instruments_in_song(file, {0, 48, 25, 33, 30, 128}) > 2):
                try:
                    shutil.copy(file, "F:/ADELA/studia/MAGISTERKA/data/various_4_4_timeSignature_6instruments/")
                except:
                    pass

    @classmethod
    def in_song_save_only_chosen_instruments(cls, data_dir, list_of_instruments_to_left):
        ''' List of instrument to left for biggest midi collection: {33, 25, 48, 0} + drum
            List of instrument to left for MUZA_MIDI collection: {87, 48, 27, 25} + drum '''
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            fileName = file.split('\\')[-1]
            file = pretty_midi.PrettyMIDI(file)
            originalInstruments = file.instruments
            chosenInstruments = []
            file.instruments = []
            for i in originalInstruments:
                if i.program in list_of_instruments_to_left or i.is_drum:
                    chosenInstruments.append(i)
            file.instruments = chosenInstruments
            file.write("F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_test_dataset_len96/" + fileName)

    @classmethod
    def in_song_save_only_chosen_instruments_no_drum(cls, data_dir, list_of_instruments_to_left):
        ''' List of instrument to left for biggest midi collection: {33, 25, 48, 0} + drum
            List of instrument to left for MUZA_MIDI collection: {87, 48, 27, 25} + drum '''
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            fileName = file.split('\\')[-1]
            file = pretty_midi.PrettyMIDI(file)
            originalInstruments = file.instruments
            chosenInstruments = []
            file.instruments = []
            for i in originalInstruments:
                if i.program in list_of_instruments_to_left or i.is_drum:
                    chosenInstruments.append(i)
            file.instruments = chosenInstruments
            file.write("F:/ADELA/studia/MAGISTERKA/data/MUZA_MIDI_chosenSongsWithOnly4ChosenInstrumentsNoDrum/" + fileName)

    @classmethod
    def copy_songs_with_given_number_of_instruments(cls, data_dir, output_dir, number_of_instruments):
        for file in tqdm(glob.glob(data_dir, recursive=True)):
            song = pretty_midi.PrettyMIDI(file)
            multitrack = pypianoroll.from_pretty_midi(song)
            programs_set = set()
            for t in multitrack.tracks:
                programs_set.add(t.program)
            if len(programs_set) >= number_of_instruments:
                try:
                    shutil.copy(file, output_dir)
                except:
                    print("except")