
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, GRU, Dense, TimeDistributed, Reshape


class Autoencoder():

    def __init__(self):
        self.chords_len = 128  # length of one-hot encoded vector representing one note or chord - 128
        self.gru_state = 1024  # length of wide lstm cells output
        self.num_of_instrumets = 6

        self.encoder, self.decoder, self.model = self.autoencoder_model()

    def autoencoder_model(self):
        enc_inputs1 = Input(shape=(None, self.num_of_instrumets, self.chords_len), name="encoder_input")
        enc_inputs2 = Reshape((-1, self.chords_len * self.num_of_instrumets),
                              input_shape=(None, self.num_of_instrumets, self.chords_len))(enc_inputs1)
        enc_output = GRU(self.gru_state, return_sequences=False, return_state=False, name="gru_1")(enc_inputs2)
        encoder = Model(enc_inputs1, enc_output, name="encoder")

        # define decoder
        dec_inputs = Input(shape=encoder.output.shape[1:], name="decoder_input")
        dec_inputs_exp = tf.expand_dims(dec_inputs, axis=1, name="expand_dims")
        dec_repeated = tf.tile(dec_inputs_exp, [1, tf.shape(enc_inputs1)[1], 1], name="tile")
        dec_gru = GRU(self.gru_state, return_sequences=True, name="gru_2")(dec_repeated)
        instr0 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr0")(dec_gru)
        instr25 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr25")(dec_gru)
        instr30 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr30")(dec_gru)
        instr33 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr33")(dec_gru)
        instr48 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr48")(dec_gru)
        instr128 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr128")(dec_gru)
        # [0]: 0, [1]: 25, [2]: 33, [3]: 48, [4]: 128
        concat_output = tf.stack([instr0, instr25, instr30, instr33, instr48, instr128], axis=-2,
                                 name="concatenated_output")

        decoder = Model(inputs=[dec_inputs, enc_inputs1], outputs=concat_output, name="decoder")

        model_encoded = encoder(enc_inputs1)
        model_decoded = decoder([model_encoded, enc_inputs1])
        model = Model(enc_inputs1, outputs=model_decoded, name="autoencoder")

        return encoder, decoder, model

    def load_my_model(self, model_weights):
        # load previously saved weights to the model
        self.model.load_weights(model_weights)