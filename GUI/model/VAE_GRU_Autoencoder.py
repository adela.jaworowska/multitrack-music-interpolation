import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, LSTM, GRU, Dense, Lambda, TimeDistributed, Reshape

# =========================
# Define custom loss
# VAE is trained using two loss functions reconstruction loss and KL divergence
# Let us add a class to define a custom layer with loss
class CustomLossLayer(keras.layers.Layer):

    def vae_loss(self, x, z_decoded, z_sigma, z_mu):
        x = K.flatten(x)
        z_decoded = K.flatten(z_decoded)

        # Reconstruction loss (as we used sigmoid activation we can use binarycrossentropy)
        recon_loss = 73728 * keras.metrics.binary_crossentropy(x, z_decoded)

        # KL divergence
        kl_loss = -0.5 * K.sum(1 + z_sigma - K.square(z_mu) - K.exp(z_sigma), axis=-1)
        return K.mean(recon_loss + kl_loss)

    # add custom loss to the class
    def call(self, inputs, z_sigma, z_mu):
        x = inputs[0]
        z_decoded = inputs[1]
        loss = self.vae_loss(x, z_decoded, z_sigma, z_mu)
        self.add_loss(loss, inputs=inputs)
        return z_decoded

class Autoencoder():

    def __init__(self):
        self.chords_len = 128  # length of one-hot encoded vector representing one note or chord - 128
        self.gru_state = 1024  # config.gru_inner_state # length of wide lstm cells output
        self.num_of_instrumets = 6
        self.custom_loss_layer = CustomLossLayer()

        self.encoder, self.decoder, self.model = self.autoencoder_model()

    def sample_z(self, args):
        z_mu, z_sigma = args
        eps = K.random_normal(shape=(K.shape(z_mu)[0], K.int_shape(z_mu)[1]))
        return z_mu + K.exp(z_sigma / 2) * eps

    def autoencoder_model(self):
        enc_inputs1 = Input(shape=(None, self.num_of_instrumets, self.chords_len), name="encoder_input")
        enc_inputs2 = Reshape((-1, self.chords_len * self.num_of_instrumets),
                              input_shape=(None, self.num_of_instrumets, self.chords_len))(enc_inputs1)
        enc_output = GRU(self.gru_state, return_sequences=False, return_state=False, name="gru_2")(enc_inputs2)
        z_mu = Dense(self.gru_state, name='latent_mu')(enc_output)  # Mean values of encoded input
        z_sigma = Dense(self.gru_state, name='latent_sigma')(enc_output)  # Std dev. (variance) of encoded input
        z = Lambda(self.sample_z, output_shape=(self.gru_state,), name='z')([z_mu, z_sigma])
        encoder = Model(enc_inputs1, [z_mu, z_sigma, z], name="encoder")

        # define decoder
        # dec_inputs = Input(shape=encoder.output.shape[1:], name="decoder_input")
        dec_inputs = Input(shape=(self.gru_state), name="decoder_input")
        dec_inputs_exp = tf.expand_dims(dec_inputs, axis=1, name="expand_dims")
        dec_repeated = tf.tile(dec_inputs_exp, [1, tf.shape(enc_inputs1)[1], 1],
                               name="tile")  # tf.shape(enc_inputs1)[1] -> 96
        dec_gru = GRU(self.gru_state, return_sequences=True, name="gru_3")(dec_repeated)
        instr0 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr0")(dec_gru)
        instr25 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr25")(dec_gru)
        instr30 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr30")(dec_gru)
        instr33 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr33")(dec_gru)
        instr48 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr48")(dec_gru)
        instr128 = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="instr128")(dec_gru)
        # [0]: 0, [1]: 25, [2]: 33, [3]: 48, [4]: 128
        concat_output = tf.stack([instr0, instr25, instr30, instr33, instr48, instr128], axis=-2,
                                 name="concatenated_output")

        decoder = Model(inputs=[dec_inputs, enc_inputs1], outputs=concat_output, name="decoder")
        # decoder = Model(inputs=[dec_inputs, enc_inputs1], outputs=concat_output, name="decoder")

        # model_encoded = encoder(enc_inputs1)
        model_decoded = decoder([z, enc_inputs1])
        y = self.custom_loss_layer([enc_inputs1, model_decoded], z_sigma, z_mu)

        # model = Model(enc_inputs1, outputs=model_decoded, name="autoencoder")
        model = Model(enc_inputs1, outputs=y, name="autoencoder")

        return encoder, decoder, model

    def load_my_model(self, model_weights):
        self.model.load_weights(model_weights)