

Multitrack Music Interpolation
-
Pliki znajdujące się w tym folderze stanowią załącznik do pracy magisterskiej.  

Politechnika Warszawska, Wydział Elektroniki Technik Informacyjnych, Instytut Informatyki  
tytuł pracy: Porównanie działania sieci neuronowych w interpolacji wielościeżkowych struktur muzycznych   
promotor: prof. dr hab. inż. Przemysław Rokita  
autor: Adela Jaworowska, 283747  

### Struktura katalogu to:

**/video** - Folder zawiera wideo, pokazujące działanie aplikacji.

**/interpolation_output_examples** - Folder zawiera przykłady otrzymanych w wyniku interpolacji rezultatów (MIDI i PNG). W każdym z podfolderów znajdują się rezultaty otrzymane w wyniku innego eksperymentu.
- **/1_allModels** - porównanie interpolacji dwóch tych samych fragmentów wejściowych z wykorzystaniem różnych modeli sieci (AE z LSTM, AE z GRU, VAE z LSTM, VAE z GRU)
- **/2_LERPvsSLERP** - porównanie interpolacji z wykorzystaniem wzoru na interpolację liniową (LERP) i interpolację sferyczną (SLERP)
- **/3_different_instruments_numbers** - porównanie interpolacji utworów wejściowych zawierających różną liczbą instrumentów. Odpowiednie podfoldery zawierają wyniki interpolacji utworów z 3, 4, 5, 6 instrumentami.
- **/4_sameParameters_differentOutpuyByVAE** - kilka wyników interpolacji tych samych utworów wejściowych z wykorzystaniem tych samych parametrów dla modelu VAE z warstwami LSTM. Eksperyment pokazuję, że element generatywny w VAE powoduje wygenerowanie za każdym razem delikatnie innego rezultatu.
- **/5_LOFI** - przykładowy rezultat interpolacji z wykorzystaniem niepoprawnie wytrenowanego modelu (zbyt mała uwaga była przyłożona do poprawnej rekonstrukcji danych). Wyniki delikatnie przypominają muzykę lo-fi. 
- **/6_differentInterpolationLengths** - interpolacja utworów z różną długością interpolacji

#### /GUI

- **main.py** - główny plik służacy do uruchomienia aplikacji graficznej
- **window.py** - plik w którym zdefiniowany jest widok i logika aplikacji
- **config.py** - plik konfiguracyjny aplikacji

- **/model** - Folder zawiera kod modeli neuronowych oraz folder z wagami wytrenowanych modeli:
	- AE_LSTM_Autoencoder.py
	- AE_GRU_Autoencoder.py
	- VAE_LSTM_Autoencoder.py
	- VAE_GRU_Autoencoder.py
	- **/weights** - folder zawiera wagi wytrenowanych modeli neuronowych: 
		- AE LSTM.h5
		- AE GRU.h5
		- VAE LSTM.h5
		- VAE GRU.h5
		- VAE lofi.h5

- **/exampleSamples** - Folder zawiera przykładowe pliki muzyczne w formacie MIDI, które mogą być interpolowane w aplikacji.

- **midi_scripts/**
	- interpolator.py - klasa odpowiadająca za interpolację 
	- midi_preprocessing.py - klasa odpowiadająca ze przetwarzanie danych w formacie MIDI
	
- **/interpolationOutput** - W tym folderze zapisywane są pliki po interpolacji dokonywanej w aplikacji.
	
### environment.yml
Plik zawiera wszystkie pakiety i biblioteki wraz z wersjami, które były w wykorzystywanym środowiku narzedzia conda. Należy je zainstalować w środowisku języka python aby możliwe było uruchomienie aplikacji.

### INSTRUKCJA:
Aby uruchomić aplikację graficzną należy:
1. Zainstalować narzędzie Anaconda - zarządce pakietów języka python (https://docs.anaconda.com/anaconda/install/index.html)
2. Za pomocą Anacondy stworzyć środowisko: conda create --name myenv
3. Zainstalowac w stworzonym środowisku wszystkie wymagane pakiety, podane w pliku envirnment.yml
4. Zainstalować program PyCharm
5. W programie PyCharm otworzyć projekt i ustawić jego interpreter na plik python.exe z katalogu, w którym zainstalowano wcześniej środowisko condy
6. Uruchomić plik GUI/main.py 
